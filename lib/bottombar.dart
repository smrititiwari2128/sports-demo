import 'package:flutter/material.dart';
import 'package:sport/Notifications.dart';
import 'package:sport/Search.dart';

class BottomBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
        //notchMargin: 20.0,
        color: Colors.transparent,
        elevation: 10.0,
        clipBehavior: Clip.none,
        child: Container(
            height: 50.0,
            color: Color(0xFFF4FCFF),
            child: Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      height: 50.0,
                      width: MediaQuery.of(context).size.width / 2,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            width: 100,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Color(0xff2CA1AE),
                            ),
                            child: Row(
                              children: [
                                IconButton(
                                    icon: new Icon(
                                      Icons.search,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Search()));
                                    }),
                                //Icon(Icons.search, color: Colors.white),
                                Text(
                                  "Explore",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          IconButton(
                              icon: new Icon(Icons.notifications,
                                  color: Color(0xff2CA1AE)),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Note()));
                              }),
                        ],
                      )),
                  Container(
                    child: Row(children: [
                      IconButton(
                          icon: new Icon(Icons.check_circle,
                              color: Color(0xff2CA1AE)),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Note()));
                          }),
                    ]),
                  ),
                  Container(
                      height: 50.0,
                      width: MediaQuery.of(context).size.width / 2.5 - 20,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(Icons.shopping_cart, color: Color(0xff2CA1AE)),
                          Icon(Icons.person, color: Color(0xff2CA1AE))
                        ],
                      )),
                ])));
  }
}
