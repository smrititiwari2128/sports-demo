import 'package:flutter/material.dart';
import 'package:sport/Bookings.dart';
import 'package:sport/Cancel.dart';
import 'package:sport/Games.dart';
import 'package:sport/Games_Rep.dart';
import 'package:sport/Name.dart';
import 'package:sport/No_show.dart';
import 'package:sport/OpaqueImage.dart';
import 'package:sport/stackedcontainer.dart';
import 'bottombar.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //List<Games> allGames;
  @override
  Widget build(BuildContext context) {
    //var size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: [
            Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height * 0.4,
                  width: double.infinity,
                  // decoration: BoxDecoration(
                  //   image: DecorationImage(image: AssetImage("assets/gr.jpg")),
                  // ),
                  child: Stack(
                    children: <Widget>[
                      OpaqueImage(
                        imageUrl: "assets/gr.jpg",
                      ),
                      Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CircleAvatar(
                                  radius: 50,
                                  backgroundImage: AssetImage(
                                    "assets/pp.jpg",
                                  ),
                                ),
                                SizedBox(width: 40),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "BisBas",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text("Bishow Basnet",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold))
                                  ],
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                    right: 0,
                    left: 0,
                    bottom: -40,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Book()));
                          },
                          child: StackedContainer(
                            label: "Bookings",
                            count: "45",
                            badge: 1,
                          ),
                        ),
                        GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Cancel()));
                            },
                            child: StackedContainer(
                                label: "Cancelled", count: "5")),
                        GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => No()));
                            },
                            child:
                                StackedContainer(label: "No show", count: "5"))
                      ],
                    )),
              ],
            ),
            SizedBox(height: 40),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Name()));
              },
              child: Container(
                height: 100,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: allGames.length,
                  itemBuilder: (context, i) {
                    return Container(
                      width: 180,
                      child: Card(
                        color: Colors.white,
                        child: Stack(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Positioned(
                              left: 0,
                              top: 10,
                              child: Container(
                                height: 80,
                                width: 80,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.green[100],
                                ),
                                child: CircleAvatar(
                                  radius: 30,
                                  backgroundImage: AssetImage(
                                      allGames[i].imageUrl.toString()),
                                ),
                              ),
                            ),
                            Positioned(
                                top: 10,
                                right: 0,
                                child: Text(
                                  allGames[i].name,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                )),
                            Positioned(
                                top: 40,
                                right: 0,
                                child: Text(allGames[i].label))
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            Container(
              height: 300,
              child: Card(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 2),
                          child: Text(
                            "ABOUT",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        Container(
                            height: 120,
                            child: Image.asset(
                              "assets/field.png",
                              fit: BoxFit.fitWidth,
                            )),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 30,
                        ),
                        Text("FullName"),
                        Text(
                          "Bishow Basnet",
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text("username"),
                        Text(
                          "BisBas",
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text("BirthDate"),
                        Text(
                          "1997/09/17",
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text("password"),
                        Text(
                          "********",
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text("Member Since"),
                        Text("3 May,2020", style: TextStyle(fontSize: 20)),
                        SizedBox(
                          height: 5,
                        ),
                        Text("Address"),
                        Text("babarmahal", style: TextStyle(fontSize: 20)),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Card(
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                        image: AssetImage(
                          "assets/we.PNG",
                        ),
                        fit: BoxFit.fitWidth)),
              ),
            ),
            BottomBar(),
          ],
        ),
      ),
    );
  }
}
