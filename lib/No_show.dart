import 'package:flutter/material.dart';

class No extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.pink[100],
      child: Align(
        alignment: Alignment.center,
        child: Text(
          "NO SHOWS",
          style: TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.bold,
              color: Colors.pinkAccent),
        ),
      ),
    );
  }
}
