import 'package:flutter/material.dart';

class StackedContainer extends StatelessWidget {
  final label, count, badge;
  const StackedContainer({Key key, this.label, this.count, this.badge})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      overflow: Overflow.visible,
      children: [
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12, offset: Offset(0, 2), blurRadius: 5)
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("$label",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14,
                          fontWeight: FontWeight.bold)),
                  SizedBox(width: 20),
                ],
              ),
              SizedBox(height: 10),
              Text(
                "$count",
                style: TextStyle(
                  color: Colors.blue[900],
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.end,
              ),
            ],
          ),
        ),
        if (badge != null)
          Positioned(
              top: -5,
              right: 0,
              child: Container(
                  padding: EdgeInsets.all(8),
                  decoration:
                      BoxDecoration(shape: BoxShape.circle, color: Colors.red),
                  child: Text(
                    "$badge",
                    style: TextStyle(color: Colors.white),
                  )))
      ],
    );
  }
}
