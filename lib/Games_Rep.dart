import 'package:sport/Games.dart';

List<Games> allGames = [
  Games(
      name: 'Futsal',
      label: 'Goalkeeper',
      imageUrl: 'assets/fu.png',
      game: 'futsal'),
  Games(
      name: 'Football',
      label: 'Forward',
      imageUrl: 'assets/gr.jpg',
      game: 'football'),
  Games(
      name: 'Basketball',
      label: 'forward',
      imageUrl: 'assets/bb.jpg',
      game: 'Basketball'),
];
