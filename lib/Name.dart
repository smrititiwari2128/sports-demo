import 'package:flutter/material.dart';
import 'package:sport/Games.dart';
import 'package:sport/Games_Rep.dart';

class Name extends StatelessWidget {
  // List<Games> allGames;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: allGames.length,
      itemBuilder: (context, i) {
        return Align(
          alignment: Alignment.center,
          child: Container(
            padding: EdgeInsets.all(30),
            color: Colors.pink[100],
            child: SizedBox(
              height: 100,
              child: Text(
                allGames[i].game,
                style: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                    color: Colors.pinkAccent),
              ),
            ),
          ),
        );
      },
    );
  }
}
