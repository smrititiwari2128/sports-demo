import 'package:flutter/material.dart';

class Cancel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.pink[100],
      child: Align(
        alignment: Alignment.center,
        child: Text(
          "CANCELLED",
          style: TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.bold,
              color: Colors.pinkAccent),
        ),
      ),
    );
  }
}
